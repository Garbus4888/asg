import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

import { Matches } from '../../api/matches.js';

// Import to load these templates
import '../../ui/layouts/main_layout.js';
import '../../ui/pages/home_page.js';
import '../../ui/pages/register_page.js';
import '../../ui/pages/login_page.js';
import '../../ui/pages/match_page.js';
import '../../ui/pages/score_page.js';

import '../../ui/pages/admin/dashboard_page.js';
import '../../ui/pages/admin/add_match_page.js';
import '../../ui/pages/admin/add_user_page.js';

const adminRoutes = FlowRouter.group({
  prefix: '/admin',
  name: 'admin',
  triggersEnter: [function() {
    console.log('admin router');
  }]
});

adminRoutes.route('/', {
  action: function() {
    BlazeLayout.render('MainLayout', {content: 'DashboardPage'});
  }
});

adminRoutes.route('/dashboard', {
  action: function() {
    BlazeLayout.render('MainLayout', {content: 'DashboardPage'});
  }
});

adminRoutes.route('/add-match', {
  action: function() {
    BlazeLayout.render('MainLayout', {content: 'AddMatchPage'});
  }
});

adminRoutes.route('/add-user', {
  action: function() {
    BlazeLayout.render('MainLayout', {content: 'addUserPage'});
  }
});

FlowRouter.route('/', {
  action: function() {
    BlazeLayout.render("MainLayout", {content: "HomePage"});
  }
});

FlowRouter.route('/register', {
  action: function() {
    BlazeLayout.render('MainLayout', {content: 'RegisterPage'});
  }
});

FlowRouter.route('/login', {
  action: function() {
    BlazeLayout.render('MainLayout', {content: 'LoginPage'});
  }
});

FlowRouter.route('/scores', {
  action: function() {
    BlazeLayout.render('MainLayout', {content: 'scorePage'});
  }
});

FlowRouter.route('/match/:matchId', {
  action: function(params) {
    const match = Matches.findOne({_id: params.matchId});
    BlazeLayout.render('MainLayout', {content: 'matchPage', tests: params.matchId});
  }
});
