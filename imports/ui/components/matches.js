import {Template} from 'meteor/templating';

import './matches.html';

import { Matches } from '../../api/matches.js';

Template.matches.onCreated(function matchesOnCreated() {
  Meteor.subscribe('matches');
});

// console.log(Matches.find());
Template.matches.helpers({
  matches: function() {
    return Matches.find();
  }
});
