import { Meteor } from 'meteor/meteor';

import './users.html';

Template.users.onCreated(function usersOnCreated() {
  this.autorun(() => {
    this.subscribe('users')
  });
});

console.log(Meteor.users.find().fetch());
Template.users.helpers({
  users: function() {
    return Meteor.users.find();
  }
})
