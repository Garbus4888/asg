import './matches.html';

import { Matches } from '../../../api/matches.js';

Template.matchesAdmin.onCreated(function matchesAdminOnCreated() {
  Meteor.subscribe('matches');
});

Template.matchesAdmin.helpers({
  matches: function() {
    return Matches.find();
  }
});

Template.matchesAdmin.events({
  'click #remove-match'(event) {
    event.preventDefault();

    Matches.remove(this._id);
  }
})
