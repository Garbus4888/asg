import {Template} from 'meteor/templating';

import './body.html';

Template.body.helpers({
  matches: [
    {
      name: 'Mecz 1',
      date: '18-01-2016'
    },
    {
      name: 'Mecz 2',
      date: '18-02-2016'
    },
    {
      name: 'Mecz 3',
      date: '18-03-2016'
    }
  ]
});
