import {Template} from 'meteor/templating';
import {Meteor} from 'meteor/meteor';

import './main_layout.html';

Template.MainLayout.events({
  'click #logout': function(event) {
    Meteor.logout();
  }
});
