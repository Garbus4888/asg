import './register_page.html';
import { ReactiveDict } from 'meteor/reactive-dict';


Template.RegisterPage.onCreated(function() {
  this.state = new ReactiveDict();
  this.state.set('REGISTER_SUCCESS', null);
  this.state.set('REGISTER_ERROR', null);
})

Template.RegisterPage.helpers({
  registerSuccess: function() {
    return Template.instance().state.get('REGISTER_SUCCESS')
  },
  registerError: function() {
    return Template.instance().state.get('REGISTER_ERROR')
  }
})


Template.RegisterPage.events({
  'submit .register': function(event, template) {
     event.preventDefault();

     const target = event.target;
     const username = target.username.value;
     const password = target.password.value;

     Accounts.createUser({username, password}, function(error) {
       if(error) {
         template.state.set('REGISTER_ERROR', true);
       } else {
         template.state.set('REGISTER_SUCCESS', true);
       }
     })
   }
});
