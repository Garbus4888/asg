import '../../components/player.js';
import './add_match_page.html';
import {Template} from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';
import { Matches } from '../../../api/matches.js';
// import { PlayersLocal } from '../../../api/players_local.js';

Template.AddMatchPage.onCreated(function() {
  this.state = new ReactiveDict();
  this.state.set('TEAM_ONE', []);
  this.state.set('TEAM_TWO', []);
  this.state.set('ADD_MATCH_SUCCESS', null);
  this.state.set('ADD_MATCH_ERROR', null);

  this.autorun(() => {
    this.subscribe('players');
  });

  Meteor.users.find().fetch().map((player) => {
    playersLocal.insert({_id: player._id, username: player.username});
  });
})

const playersLocal = new Mongo.Collection(null);
const matchPlayersLocal = new Mongo.Collection(null);
const teamOneLocal = new Mongo.Collection(null);
const teamTwoLocal = new Mongo.Collection(null);
const Match = new Mongo.Collection(null);



Template.AddMatchPage.helpers({
  players: function() {
    return playersLocal.find();
  },
  teamOne: function() {
    return matchPlayersLocal.find({team: 1});
  },
  teamTwo: function() {
    return matchPlayersLocal.find({team: 2});
  },
  addMatchSuccess: function() {
    return Template.instance().state.get('ADD_MATCH_SUCCESS');
  },
  addMatchError: function() {
    return Template.instance().state.get('ADD_MATCH_ERROR');
  }
});

Template.AddMatchPage.events({
  'click #add-player-1'(event, template) {
    event.preventDefault();

    const playerId = $('#team-one-dropdown').dropdown('get value');

    const player = playersLocal.findOne({_id: playerId});
    playersLocal.remove(playerId);

    matchPlayersLocal.insert({_id: player._id, team: 1, hits: 0, deads: 0, score: 0});

    $('#team-one-dropdown').dropdown('restore defaults');
  },
  'click #add-player-2'(event, template) {
    event.preventDefault();

    const playerId = $('#team-two-dropdown').dropdown('get value');

    const player = playersLocal.findOne({_id: playerId});
    playersLocal.remove(playerId);

    matchPlayersLocal.insert({_id: player._id, team: 2, hits: 0, deads: 0, score: 0});

    $('#team-two-dropdown').dropdown('restore defaults');
  },
  'submit #add-match'(event, template) {
    event.preventDefault();

    const target = event.target;
    const name = target.name.value;
    const date = target.date.value;
    const hour = target.hour.value;
    const place = target.place.value;

    console.log(name);
    const matchPlayers = matchPlayersLocal.find().fetch()
    const teamOne = teamOneLocal.find().fetch();
    const teamTwo = teamTwoLocal.find().fetch();

    Meteor.call('matches.insert',
      name,
      date,
      hour,
      place,
      matchPlayers,
      function(error) {
        if(error) {
          template.state.set('ADD_MATCH_ERROR', true);
        } else {
          template.state.set('ADD_MATCH_SUCCESS', true);
        }
      });
    // Matches.insert({name: name, date: date, hour: hour, place: place, teamOne: teamOne, teamTwo: teamTwo}, function(error) {
    //
    // });
  },
  'click #remove-player-1'(event) {
    event.preventDefault();

    teamOneLocal.remove(this);
    playersLocal.insert(this);
  },
  'click #remove-player-2'(event) {
    event.preventDefault();

    teamTwoLocal.remove(this);
    playersLocal.insert(this);
  }
})
