import './add_user_page.html';

Template.addUserPage.events({
  'submit #add-user'(event) {
    event.preventDefault();

    const target = event.target;
    const username = target.username.value;
    const password = target.password.value;

    Accounts.createUser({username, password}, function(error) {
      if(error) {
        console.log('użytkownik nie został stworzony');
      } else {
        FlowRouter.go('/dashboard');
      }
    });
  }
})
