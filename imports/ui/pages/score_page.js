import './score_page.html';

Template.scorePage.onCreated(function() {
  this.autorun(() => {
    this.subscribe('users')
  });
})

Template.scorePage.helpers({
  users() {
    return Meteor.users.find();
  },
  score() {
    if(!this.hits || !this.deads) {
      return 0;
    } else {
      return this.hits - this.deads;
    }
  },
  hits() {
    if(!this.hits) {
      return 0
    } else {
      return this.hits;
    }
  },
  deads() {
    if(!this.deads) {
      return 0;
    } else {
      return this.deads;
    }
  }
})
