import './login_page.html';
import {Meteor} from 'meteor/meteor';

Template.LoginPage.events({
  'submit #login-form': function(event) {
     event.preventDefault();

     const target = event.target;
     const username = target.username.value;
     const password = target.password.value;

     console.log(username + ' - ' + password)
     Meteor.loginWithPassword(username, password, function(error) {
       if(error) {
         console.log('error');
       } else {
         console.log('good');
       }
     })
  }
});
