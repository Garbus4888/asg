import './match_page.html';
import { FlowRouter } from 'meteor/kadira:flow-router';

import '../components/match_player.js';
import '../../api/matches.js';
import { Matches } from '../../api/matches.js';

Template.matchPage.onCreated(function() {
  const matchId = FlowRouter.getParam('matchId');
  this.state = new ReactiveDict();

  this.autorun(() => {
    this.subscribe('match', matchId)
  });
});

Template.matchPage.helpers({
  match: function() {
    return Matches.findOne({_id: FlowRouter.getParam('matchId')});
  },
  username: function() {
    const user = Meteor.users.findOne({_id: this._id}).username;
    return user;
  },
  team() {
    console.log(Meteor.userId());
    const match = Matches.findOne({_id: FlowRouter.getParam('matchId'), 'players._id': Meteor.userId()});
    console.log(match);
  }
});

Template.matchPage.events({
  'click #hit'(event) {
    event.preventDefault();

    Meteor.call('match.hit', FlowRouter.getParam('matchId'), function(error) {
      if(error) {
        console.log('error');
      } else {
        console.log('trafiłeś');
      }
    });
  },
  'click #hitted'(event) {
    event.preventDefault();

    Meteor.call('match.hitted', FlowRouter.getParam('matchId'), function(error) {
      if(error) {
        console.log('error');
      } else {
        console.log('trafiłeś');
      }
    });
  }
})
