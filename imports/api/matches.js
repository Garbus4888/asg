import { Mongo } from 'meteor/mongo';
import { Meteor } from 'meteor/meteor'

export const Matches = new Mongo.Collection('matches');


if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish('matches', function matchesPublication() {
    return Matches.find();
  });

  Meteor.publish('match', function(matchId) {
    return Matches.find({_id: matchId});
  })
}


Meteor.methods({
  'matches.insert'(name, date, hour, place, matchPlayers) {

    if(name.length == 0) {
      throw new Meteor.Error('Name is empty');
    }

    Matches.insert({
      name: name,
      date: date,
      hour: hour,
      place: place,
      score: {
        teamOne: 0,
        teamTwo: 0
      },
      players: matchPlayers
    })
  },
  'match.hit'(matchId) {
    // Matches.update({_id: matchId, players: {_id: this.userId}}, {$inc: {'score.teamOne': 1}})
    Matches.update({_id: matchId, 'players._id': this.userId}, {$inc: {'score.teamOne': 1, 'players.$.hits': 1, 'players.$.score': 1}})
    Meteor.users.update({_id: this.userId}, {$inc: {hits: 1}})
  },
  'match.hitted'(matchId) {
    Matches.update({_id: matchId, 'players._id': this.userId}, {$inc: {'score.teamOne': -1, 'players.$.deads': 1, 'players.$.score': -1}})
    Meteor.users.update({_id: this.userId}, {$inc: {deads: 1}})  }
})
